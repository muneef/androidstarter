document.addEventListener('DOMContentLoaded', function() {
  // do your setup here
  console.log('Initialized app');

  var Sticky = require('javascripts/sticky.min.js');
  var sticky = new Sticky('.box-therapy');

  var list = [
    {
      id: 1,
      name: 'Clean Architecture',
      desc: 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using ‘Content here, content here’, making it look like readable English.',
      license: 'MIT',
      gh: 'http://gh.com/u/androistarts/androistarts-kotlin'
    },
    {
      id: 2,
      name: 'MVP Architecture',
      license: 'MIT',
      desc: 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using ‘Content here, content here’, making it look like readable English.',
      gh: 'http://gh.com/u/androistarts/androistarts-kotlin'
    },
    {
      id: 3,
      name: 'MVVM Architecture',
      license: 'MIT',
      desc: 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using ‘Content here, content here’, making it look like readable English.',
      gh: 'http://gh.com/u/androistarts/androistarts-kotlin'
    },
    {
      id: 4,
      name: 'Koltin MVP',
      license: 'MIT',
      desc: 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using ‘Content here, content here’, making it look like readable English.',
      gh: 'http://gh.com/u/androistarts/androistarts-kotlin'
    },
  ];

  
  var app = new Vue({
    el: '#app',
    data: {
      message: 'Hello Vue!',
      items: list,
      company: 'Acme',
      project: 'Gotham',
      selected: {},
      building: false,
      build_text: 'Build'
    },
    methods: {
      selectPackage: function(e){
        console.log('selected');
        console.log(e);
        this.selected = e;
        this.building = false;
        this.build_text = 'Build';
      },
      buildNow: function(e){
        console.log('building ...');
        this.building = true;
        this.build_text = 'Building .. ';
      }
    },
  })

});
