'use strict';

const nav = [
  {title: 'Home', path: ''},
  {title: 'Docs', path: 'docs/getting-started', activeOn: 'docs/'},
  {title: 'Plugins', path: 'plugins'},
  {title: 'Skeletons', path: 'skeletons'},
  {title: 'In Production', path: 'examples'},
  {title: 'Community', path: 'support'},
];

// See http://brunch.io for documentation.
exports.files = {
  javascripts: {
    joinTo: 'app.js'
  },
  stylesheets: {
    joinTo: {
      'app.css': /^app/
    }
  }
};

exports.plugins = {
  jade: {
    locals: {nav},
  },
  sass: {
    options: {
      includePaths: [                    
          'bower_components/compass-mixins/lib',
          'bower_components/compass-breakpoint/stylesheets',          
          'bower_components/modular-scale/stylesheets',
          'bower_components/bem-constructor/dist',
          'bower_components/susy/sass',          
        ]
    }
  }
}